function [Dx, Dy, R, B] = train(X, Y, param, L)

fprintf('training...\n');

%% set the parameters
nbits = param.nbits;
alphaX = param.alphaX;
alphaY = 1-alphaX;
beta = param.beta;
lambdaX = param.lambdaX;
lambdaY = lambdaX;
mu=param.mu;
xi = param.xi;
thetaX=param.thetaX;
thetaY=thetaX;
Nu=param.Nu;


%% get the dimensions
[n, dX] = size(X);
dY = size(Y,2);

%% transpose the matrices
X = X'; Y = Y'; L = L';

%% initialization
V = randn(nbits, n);
C = randn(nbits, n);
Dx = randn(nbits, dX);
Dy = randn(nbits, dY);
E = randn(nbits, n);
F = randn(nbits, n);
Gx = randn(nbits, n);
Gy = randn(nbits, n);
R = randn(nbits, nbits);
%[U11, ~, ~] = svd(R);
%R = U11(:,1:nbits);

%% iterative optimization
for iter = 1:param.iter

    % update B
    C=alphaX*(X-V'-Dx*V*V')/(beta*eye(nbits)+alphaX*V*V')+alphaY*(X-V'-Dx*V*V')/(beta*eye(nbits)+alphaY*V*V');
    
    % update G
    Dx=alphaX*(X-V'-C*V*V')/(lambdaX*eye(nbits)+alphaX*V*V');
    Dy=alphaY*(Y-V'-C*V*V')/(lambdaY*eye(nbits)+alphaY*V*V');
    E=mu*L*V'/(mu*V*V'+xi*eye(nbits));
    Gx=alphaX*(X-V'-C*V*V')/(lambdaX*eye(nbits)+alphaX*V*V');
    Gy=alphaY*(Y-V'-C*V*V')/(lambdaY*eye(nbits)+alphaY*V*V');
    F=mu*L*V'/(mu*V*V'+xi*eye(nbits));
    A=C+(Dx+Dy)/2;
    
    V=(A'*X+mu*E'*L)/(A'*A+mu*E'*E);
    
    %Uy = alphaY*(Y*V')/(alphaY*(V*V')+gamma*eye(nbits));
    %G = alpha*(L*V')/(alpha*(V*V')+gamma*eye(nbits));

    % update W
    %Dx = Xmu*(V*X')/(Xmu*(X*X')+gamma*eye(dX));
    %Dy = Ymu*(V*Y')/(Ymu*(Y*Y')+gamma*eye(dY));

    % update V
    %V = (lambdaX*(Ux'*Ux)+alphaY*(Uy'*Uy)+alpha*(G'*G)+(R'*R)+(Xmu+Ymu+gamma)*eye(nbits))\(lambdaX*(Ux'*X)+alphaY*(Uy'*Y)+Xmu*(Dx*X)+Ymu*(Dy*Y)+alpha*(G'*L)+(R'*B));

    % update R
    %[S1, ~, S2] = svd(B*V');
    %R = S1*S2';

    %objective function
    %J = lambdaX*norm(X-Ux*V,'fro')^2+alphaY*norm(Y-Uy*V,'fro')^2+alpha*norm(L-G*V,'fro')^2+norm(B-R*V,'fro')^2+Xmu*norm(V-Dx*X,'fro')^2+Ymu*norm(V-Dy*Y,'fro')^2;
    %fprintf('objective function value @ iter %d: %f\n', iter, J);
end
